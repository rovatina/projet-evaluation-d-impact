# Basic model, with simple control for population and year
x1 <- df %>%
  lm(formula = vulnerability ~ bonding*time + bridging*time + linking*time + pop)

# Model with controls and year fixed effect (+ expenditure/capita measures, financial strength index)
x2 <- df %>%
  lm(formula = vulnerability ~ bonding*time + bridging*time + linking*time + 
       exp_dis_relief_per_capita + exp_public_works_per_capita + 
       exp_social_assistance_per_capita + exp_fire_per_capita + pop + 
       financial_strength_index # exclude unemployment - that's already in the vulnerability variable
       )

# Model with extended controls and year fixed effect (+disaster, total migration)
x3 <- df %>%
  lm(formula = vulnerability ~ bonding*time + bridging*time + linking*time + 
       exp_dis_relief_per_capita + exp_public_works_per_capita + 
       exp_social_assistance_per_capita + exp_fire_per_capita + 
       financial_strength_index + pop + 
       disaster + total_migration)

# Model with extended controls and year + region fixed effect (+region)
x4 <- df %>%
  lm(formula = vulnerability ~ bonding*time + bridging*time + linking*time + 
       exp_dis_relief_per_capita + exp_public_works_per_capita + 
       exp_social_assistance_per_capita + exp_fire_per_capita + 
       financial_strength_index + pop + 
       disaster + total_migration +
       region)

df_lm <- df
df_lm$res <- residuals(x4)
df_lm$yhat <- fitted(x4)

ggplot(df_lm, aes(x = yhat, y = res)) +  geom_point() + labs( y = " Résidus MCO" , x ="Taux d'homicide estimé")
